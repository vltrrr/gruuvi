package capture

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"net"
	"time"
)

// ****************************************************************************
// Helpers
// ****************************************************************************

var RUUVI_MANUF_DATA []byte = []byte{0xff, 0x99, 0x04}

func read_uint16(bytes []byte) uint16 {
	return binary.BigEndian.Uint16(bytes)
}

func read_int16(bytes []byte) int16 {
	return int16(read_uint16(bytes))
}

// ****************************************************************************
// RuuviPacket
// ****************************************************************************

type RuuviPacket struct {
	raw_data     []byte
	payload_data []byte
	Timestamp    time.Time
	BDAddr       net.HardwareAddr
	Temp         float64
	Humidity     float64
	Pressure     float64
	AccX         float64
	AccY         float64
	AccZ         float64
	BatVoltage   float64
	Movement     int
	Sequence     int
	TXPwr        int
	RXPwr        int
}

func NewRuuviPacket(raw_data []byte) *RuuviPacket {
	packet := RuuviPacket{
		raw_data: raw_data,
	}
	return &packet
}

// ****************************************************************************
// Methods
// ****************************************************************************

func (packet *RuuviPacket) isValid() bool {
	if len(packet.raw_data) < 16 {
		return false
	}
	if packet.raw_data[0] != 0x04 || packet.raw_data[1] != 0x3e {
		return false
	}
	if !bytes.Contains(packet.raw_data, RUUVI_MANUF_DATA) {
		return false
	}
	return true
}

func (packet *RuuviPacket) Parse() error {
	if !packet.isValid() {
		return errors.New("Invalid packet data.")
	}

	packet.parseAddr()
	packet.Timestamp = time.Now()
	data_index := bytes.Index(packet.raw_data, RUUVI_MANUF_DATA)
	packet.payload_data = packet.raw_data[data_index+3:]

	if packet.payload_data[0] == 5 {
		if len(packet.payload_data) < 24 {
			return errors.New("Invalid payload data, too few bytes.")
		}
		packet.parseV5()
	} else {
		return errors.New(
			fmt.Sprintf("Unsupported Ruuvi dataformat version: %d",
				packet.payload_data[0]))
	}
	return nil
}

func (packet *RuuviPacket) parseAddr() {
	packet.BDAddr = make(net.HardwareAddr, 6)
	for i := 0; i < 6; i++ {
		packet.BDAddr[i] = packet.raw_data[12-i]
	}
}

func (packet *RuuviPacket) parseV5() {
	packet.Temp = float64(read_int16(packet.payload_data[1:3])) * 0.005
	packet.Humidity = float64(read_uint16(packet.payload_data[3:5])) * 0.0025
	packet.Pressure = (float64(read_uint16(packet.payload_data[5:7]))-50000.0)*0.01 + 1000.0
	packet.AccX = float64(read_int16(packet.payload_data[7:9]))
	packet.AccY = float64(read_int16(packet.payload_data[9:11]))
	packet.AccZ = float64(read_int16(packet.payload_data[11:13]))
	packet.Movement = int(packet.payload_data[15])
	packet.Sequence = int(read_uint16(packet.payload_data[16:18]))

	pwr_packed := read_uint16(packet.payload_data[13:15])
	packet.BatVoltage = float64(pwr_packed>>5)*0.001 + 1.6
	packet.TXPwr = int(pwr_packed&0x1f) - 40

	if len(packet.payload_data) >= 25 {
		packet.RXPwr = int(int8(packet.payload_data[24]))
	}
}

func (packet *RuuviPacket) print_raw() {
	fmt.Println(packet.raw_data)
}
