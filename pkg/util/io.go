package util

import (
	"bufio"
	"fmt"
	"os"
)

func OpenOutputFile(out_file string) (*bufio.Writer, *os.File) {
	var out_writer *bufio.Writer = nil
	fmt.Printf("# Opening output file: %s\n", out_file)
	out_handle, err := os.OpenFile(out_file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	CheckErr(err)

	out_writer = bufio.NewWriter(out_handle)
	return out_writer, out_handle
}
