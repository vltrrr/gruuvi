package capture

import (
	"fmt"
	"sort"
	"strings"
	"sync"
	"time"
)

type RuuviTagMap struct {
	tags map[string]RuuviPacket
	lock sync.Mutex
}

func NewRuuviTagMap() *RuuviTagMap {
	tagmap := RuuviTagMap{
		tags: make(map[string]RuuviPacket),
	}
	return &tagmap
}

func (tagmap *RuuviTagMap) Update(addr string, packet RuuviPacket) {
	tagmap.lock.Lock()
	tagmap.tags[addr] = packet
	tagmap.lock.Unlock()
}

func (tagmap *RuuviTagMap) UpdateLoop(packet_chan <-chan RuuviPacket) {
	for packet := range packet_chan {
		tagmap.Update(packet.BDAddr.String(), packet)
	}
}

func (tagmap *RuuviTagMap) sortedAddrs() []string {
	addrs := make([]string, 0, 32)
	for addr := range tagmap.tags {
		addrs = append(addrs, addr)
	}
	sort.Strings(addrs)
	return addrs
}

func (tagmap *RuuviTagMap) RenderScanLines() string {
	tagmap.lock.Lock()
	defer tagmap.lock.Unlock()

	addrs := tagmap.sortedAddrs()
	var sbuild strings.Builder

	sbuild.WriteString(
		fmt.Sprintf("\n# %s\n",
			time.Now().Format("2006-01-02 15:04:05")))
	sbuild.WriteString("# MAC                temp  humid   press  RXPwr  last_seen\n")

	for _, addr := range addrs {
		packet := tagmap.tags[addr]
		sbuild.WriteString(fmt.Sprintf(
			"%s  %6.2f  %5.2f  %6.1f  %5d  %s\n",
			packet.BDAddr.String(),
			packet.Temp,
			packet.Humidity,
			packet.Pressure,
			packet.RXPwr,
			packet.Timestamp.Format("2006-01-02 15:04:05"),
		))
	}

	return sbuild.String()
}

func (tagmap *RuuviTagMap) RenderTSVLines(clean_map bool) string {
	tagmap.lock.Lock()
	defer tagmap.lock.Unlock()

	addrs := tagmap.sortedAddrs()
	var sbuild strings.Builder

	for _, addr := range addrs {
		packet := tagmap.tags[addr]
		sbuild.WriteString(fmt.Sprintf(
			"%s\t%s\t%.2f\t%.2f\t%.2f\t%d\t%.3f\t%d\n",
			packet.Timestamp.Format(time.RFC3339),
			packet.BDAddr.String(),
			packet.Temp,
			packet.Humidity,
			packet.Pressure,
			packet.Movement,
			packet.BatVoltage,
			packet.RXPwr,
		))
		if clean_map {
			delete(tagmap.tags, addr)
		}
	}

	return sbuild.String()
}
