package capture

import (
	"fmt"
	"testing"
)

var TEST_PACKET []byte = []byte{
	0x04, 0x3e, 0x2b, 0x02, 0x01, 0x03, 0x01, 0x1c, 0x25, 0xf5, 0xdd, 0xdb, 0xdb, 0x1f, 0x02, 0x01,
	0x06, 0x1b, 0xff, 0x99, 0x04, 0x05, 0x0f, 0x1c, 0x2d, 0xe6, 0xc8, 0xb7, 0xff, 0xd8, 0xff, 0xd4,
	0x03, 0xd0, 0xb0, 0xb6, 0x13, 0x02, 0x65, 0xdb, 0xdb, 0xdd, 0xf5, 0x25, 0x1c, 0xbd,
}

func TestRuuviPacketIsValid(t *testing.T) {
	packet := NewRuuviPacket(TEST_PACKET)
	if packet.isValid() == false {
		t.Error("Packet should be valid.")
	}
}

func TestRuuviPacketParse(t *testing.T) {
	packet := NewRuuviPacket(TEST_PACKET)
	packet.Parse()

	if packet.BDAddr.String() != "db:db:dd:f5:25:1c" {
		t.Error("Invalid addr")
	}

	if packet.Temp != 19.34 {
		t.Error("Invalid addr")
	}
	if packet.Humidity != 29.375 {
		t.Error("Invalid addr")
	}
	if packet.Pressure != 1013.83 {
		t.Error(fmt.Sprintf("Invalid Pressure %.f", packet.Pressure))
	}
	if packet.Movement != 19 {
		t.Error(fmt.Sprintf("Invalid movement %d", packet.Movement))
	}
	if packet.BatVoltage != 3.013 {
		t.Error(fmt.Sprintf("Invalid bat voltage %.3f", packet.BatVoltage))
	}
	if packet.RXPwr != -67 {
		t.Error(fmt.Sprintf("Invalid RXPwr %d", packet.RXPwr))
	}
}
