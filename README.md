# gruuvi

![alt text](img/gruuvi_screenshot_pix.png "gruuvi")

`gruuvi` is a scanner/logger for **RuuviTag** bluetooth beacon sensors written in Go. It can be used to scan nearby beacons or for example dump the sensor data of all or a single tag into `.tsv` file, for easy analysis with Python/Pandas or R.

**Currently `gruuvi` launches internally `hcitool` and `hcidump` processes to capture bluetooth data, so it must be usually run as root.**
In the future, it might use more sophisticated backend for capturing packets.

## Installation

### Requirements

- `bluez` for bluetooth dump and compatible bluetooth adapter. 
- golang for compilation, tested with `go1.16` in `linux/amd64`

In debian based linux, you can use:

```
sudo apt install bluez bluez-hcidump
```

### Compilation

```
go install gitlab.com/vltrrr/gruuvi/cmd/gruuvi@latest
```

## Usage

`gruuvi` supports several modes for different usecases:

- `dump`: Print RuuviTag packets as they arrive
- `scan`: Discover nearby beacons, prints periodically all seen beacons & info
- `tsv`: Print tab separated data or write it into a file

### `scan` example

Scan prints periodically all seen beacons & info:

```
$ sudo go/bin/gruuvi scan
# Starting scan, 5 sec interval

# 2021-02-22 21:39:37
# MAC                 temp  humid   press  RXPwr  last_seen
c2:c2:c2:c2:c2:c2    15.71  31.66  1027.8    -62  2021-02-22 21:39:36
```

### `tsv` example

- `-v` for verbose messages on startup
- `-d 60` for 60 second delay between logged
- `-o ruuvi_log` write data into `ruuvi_log.tsv`
- `-f c2:c2:c2:c2:c2:c2` filter only tags address `c2:c2:c2:c2:c2:c2`
- `tsv` to dump/log tab separated data

```
$ sudo go/bin/gruuvi -v -d 60 -o ruuvi_log -f c2:c2:c2:c2:c2:c2 tsv
# gruuvi 0.3.0 2021_02_22
# options:
# - mode: tsv
# - delay: 10 sec
# - MAC addr filter: da:da:dd:f5:25:1c
# - out_file: ruuvi_log.tsv
#
# Starting capture on hci0
# Opening output file: ruuvi_log.tsv
```