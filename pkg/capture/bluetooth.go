package capture

import (
	"bufio"
	"bytes"
	"encoding/hex"
	"gitlab.com/vltrrr/gruuvi/pkg/util"
	"net"
	"os/exec"
	"regexp"
	"strings"
)

var clean_regexp *regexp.Regexp = regexp.MustCompile(`(\s+|>)`)

func filter_cap_string(cap_str string) string {
	cap_str = clean_regexp.ReplaceAllString(cap_str, "")
	return strings.TrimSpace(cap_str)
}

func BLEScan(bl_frame_chan chan<- []byte, device string) {
	hcitool_cmd := exec.Command("hcitool", "-i", device, "lescan", "--duplicates", "--passive")
	hcidump_cmd := exec.Command("hcidump", "--raw")

	dump_reader, err := hcidump_cmd.StdoutPipe()
	util.CheckErr(err)

	hcitool_cmd.Start()
	hcidump_cmd.Start()

	reader := bufio.NewReader(dump_reader)

	for {
		cap_str, err := reader.ReadString('>')
		util.CheckErr(err)

		cap_str = filter_cap_string(cap_str)
		raw_data, err := hex.DecodeString(cap_str)
		if err != nil {
			continue
		}

		bl_frame_chan <- raw_data
	}
}

func BLEParseRuuvi(bl_frame_chan <-chan []byte, packet_chan chan<- RuuviPacket, filter_addr net.HardwareAddr) {
	for raw_data := range bl_frame_chan {
		packet := NewRuuviPacket(raw_data)

		err := packet.Parse()
		if err != nil {
			continue
		}

		if filter_addr != nil && !bytes.Equal(packet.BDAddr, filter_addr) {
			continue
		}

		packet_chan <- *packet
	}
}
