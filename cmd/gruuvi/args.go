package main

import (
	"flag"
	"fmt"
	"gitlab.com/vltrrr/gruuvi/pkg/util"
	"net"
	"strings"
)

// ****************************************************************************
// Args struct & methods
// ****************************************************************************

type GruuviArgs struct {
	mode            string
	device          string
	filter_mac_addr net.HardwareAddr
	delay           int
	out_file        string
	out_enabled     bool
	version         bool
	verbose         bool
}

func (args *GruuviArgs) parseFilterHwAddr(mac_addr string) {
	filter_addr, err := net.ParseMAC(mac_addr)
	if err != nil {
		return
	}
	args.filter_mac_addr = filter_addr
}

func (args *GruuviArgs) parseDelay() {
	if args.delay < 1 {
		args.delay = 1
	}
}

func (args *GruuviArgs) parseOutFile() {
	if args.out_file != "" {
		args.out_enabled = true
		if !strings.HasSuffix(args.out_file, ".tsv") {
			args.out_file = args.out_file + ".tsv"
		}
	}
}

// ****************************************************************************
// Parsing
// ****************************************************************************

func parseArgs() *GruuviArgs {
	devicePtr := flag.String("i", "hci0", "Bluetooth device/interface")
	filterMACAddrPtr := flag.String("f", "", "Filter BLE HW MAC address")
	delayPtr := flag.Int("d", 5, "Scan delay/interval")
	outfilePtr := flag.String("o", "", "Output also to tsv file (tsv mode only)")
	versionPtr := flag.Bool("version", false, "Display version")
	verbosePtr := flag.Bool("v", false, "Verbose, prints more info")

	flag.Parse()

	args := GruuviArgs{
		device:      *devicePtr,
		out_file:    *outfilePtr,
		out_enabled: false,
		delay:       *delayPtr,
		version:     *versionPtr,
		verbose:     *verbosePtr,
	}

	if flag.NArg() < 1 && args.version == false {
		printUsage()
		util.ExitWithError("Not enough args given")
	}
	args.mode = flag.Arg(0)

	args.parseFilterHwAddr(*filterMACAddrPtr)
	args.parseDelay()
	args.parseOutFile()

	return &args
}

func printUsage() {
	fmt.Printf("gruuvi (%s)\n", VERSION)
	fmt.Println("")
	fmt.Println("Usage:")
	fmt.Println("")
	fmt.Println("  gruuvi [options] <mode>")
	fmt.Println("")
	fmt.Println("where mode is one of: dump scan tsv")
	fmt.Println("")
	fmt.Println("Options:")

	flag.PrintDefaults()

	fmt.Println("")
}
