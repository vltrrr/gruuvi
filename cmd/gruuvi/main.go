package main

import (
	"bufio"
	"fmt"
	"gitlab.com/vltrrr/gruuvi/pkg/capture"
	"gitlab.com/vltrrr/gruuvi/pkg/util"
	"os"
	"time"
)

// ****************************************************************************
// global
// ****************************************************************************

const (
	VERSION string = "0.3.0 2021_02_22"
)

// ****************************************************************************
// modes
// ****************************************************************************

func startDumpMode(packet_chan <-chan capture.RuuviPacket) {
	for packet := range packet_chan {
		fmt.Printf(
			"%s  %s  seq=%d  temp=%.2f  humid=%.2f  press=%.2f  mov=%d  bat=%.3f  rx=%d\n",
			packet.Timestamp.Format(time.RFC3339),
			packet.BDAddr.String(),
			packet.Sequence,
			packet.Temp,
			packet.Humidity,
			packet.Pressure,
			packet.Movement,
			packet.BatVoltage,
			packet.RXPwr,
		)
	}
}

func startScanMode(packet_chan <-chan capture.RuuviPacket, args *GruuviArgs) {
	tagmap := capture.NewRuuviTagMap()
	go tagmap.UpdateLoop(packet_chan)

	fmt.Printf("# Starting scan, %d sec interval\n", args.delay)
	ticker := time.NewTicker(time.Duration(args.delay) * time.Second)

	for {
		<-ticker.C
		fmt.Print(tagmap.RenderScanLines())
	}
}

func startTSVMode(packet_chan <-chan capture.RuuviPacket, args *GruuviArgs) {
	tagmap := capture.NewRuuviTagMap()
	go tagmap.UpdateLoop(packet_chan)

	var out_writer *bufio.Writer
	var out_handle *os.File
	if args.out_enabled {
		out_writer, out_handle = util.OpenOutputFile(args.out_file)
		defer out_handle.Close()
	}

	ticker := time.NewTicker(time.Duration(args.delay) * time.Second)

	for {
		<-ticker.C
		lines := tagmap.RenderTSVLines(true)

		if args.out_enabled {
			out_writer.WriteString(lines)
			out_writer.Flush()
		} else {
			fmt.Print(lines)
		}
	}
}

// ****************************************************************************
// main
// ****************************************************************************

func main() {
	args := parseArgs()

	if args.version {
		fmt.Printf("gruuvi (%s)\n", VERSION)
		return
	}

	if args.verbose {
		fmt.Printf("# gruuvi %s\n", VERSION)
		fmt.Printf("# options:\n")
		fmt.Printf("# - mode: %s\n", args.mode)
		fmt.Printf("# - delay: %d sec\n", args.delay)

		if args.filter_mac_addr != nil {
			fmt.Printf("# - MAC addr filter: %s\n", args.filter_mac_addr)
		}
		if args.out_enabled {
			fmt.Printf("# - out_file: %s\n", args.out_file)
		}

		fmt.Printf("# \n")
		fmt.Printf("# Starting capture on %s\n", args.device)
	}

	bl_frame_chan := make(chan []byte)
	packet_chan := make(chan capture.RuuviPacket)

	go capture.BLEScan(bl_frame_chan, args.device)
	go capture.BLEParseRuuvi(bl_frame_chan, packet_chan, args.filter_mac_addr)

	switch args.mode {
	case "dump":
		startDumpMode(packet_chan)
	case "scan":
		startScanMode(packet_chan, args)
	case "tsv":
		startTSVMode(packet_chan, args)
	default:
		printUsage()
	}
}
