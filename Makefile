install:
	go install ./...

test:
	go test -v ./...

fmt:
	gofmt -w .


.PHONY: intall test fmt
