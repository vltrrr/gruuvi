package util

import (
	"fmt"
	"log"
	"os"
)

func ExitWithError(msg string) {
	fmt.Printf("Error: %s\n", msg)
	os.Exit(1)
}

func CheckErr(err error) {
	if err != nil {
		fmt.Println("\n*** Unexpected error ***")
		log.Fatal(err)
	}
}
